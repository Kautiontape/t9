#!/usr/bin/env python

import sys
import itertools
import enchant

def num2letters(num):
    return {
        '2': ['a','b','c'],
        '3': ['d','e','f'],
        '4': ['g','h','i'],
        '5': ['j','k','l'],
        '6': ['m','n','o'],
        '7': ['p','q','r','s'],
        '8': ['t','u','v'],
        '9': ['w','x','y','z']
    }[num];

def allcombos(word):
    return list(itertools.product(*word))

def map2num(word):
    return [num2letters(i) for i in word]

def genguesses(word):
    return [''.join(g) for g in allcombos(map2num(word))]

def guesses(word):
    d = enchant.Dict("en_US")
    return list(filter(lambda w: d.check(w), genguesses(word)))

def words(code):
    return [word.rstrip() for word in code.split('0')]

def solve(code):
    return [guesses(word) for word in words(code)]

def get_print_letter_combos(word):
    return ', '.join([''.join(i) for i in map2num(word)])

def print_word_answer(guesses, word):
    if len(guesses) > 0:
        print(", ".join(guesses))
    else:
        print("< {} > : {}".format(word, get_print_letter_combos(word)))

def main(i):
    for w in words(i):
        print_word_answer(guesses(w), w)

if __name__ == "__main__":
    if len(sys.argv) > 1 and sys.argv[1].isdigit():
        main(sys.argv[1])
    else:
        for line in sys.stdin:
            main(line)
