#!/usr/bin/env python

import sys
import re

def find_key(input_dict, value):
    return next((k for k, v in input_dict.items() if value in v), None)

def letters2num(letter):
    return find_key({
        '2': ['a','b','c'],
        '3': ['d','e','f'],
        '4': ['g','h','i'],
        '5': ['j','k','l'],
        '6': ['m','n','o'],
        '7': ['p','q','r','s'],
        '8': ['t','u','v'],
        '9': ['w','x','y','z'],
        '0': [' ']
    }, letter)

def cleantext(text):
    return re.sub(r'[^a-z ]+', '', text.lower())

def solve(text):
    # print("Encoding \"" + cleantext(text) + "\"")
    return [letters2num(char) for char in cleantext(text)]

if __name__ == "__main__":
    if len(sys.argv) > 1:
        print(''.join(solve(sys.argv[1])))
    else:
        print("No valid code to guess")

